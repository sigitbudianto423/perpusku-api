<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_buku extends CI_Model
{
  	private $table = "tbl_buku";

	public function getbuku($id, $idUser){
		$this->db->where('id_user', $idUser);
        if ($id == null) {
            $buku = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id_buku', $id);
            $buku = $this->db->get($this->table)->row();
        }
        return $buku;
    }

    public function updatebuku($id, $data)
        {
            $this->db->where('id_buku', $id);
            $this->db->update($this->table, $data);
        }


        public function cekId($id)
        {
            $this->db->where('id_buku', $id);
            $data = $this->db->get('tbl_buku')->row();
            return $data;
        }

        function seletctby($id){
            $data = $this->db->get_where('tbl_buku', ['id_buku'=>$id]);
            return $data;
        }



        public function deletebuku($id)
        {
            $this->db->where('id_buku', $id);
            $this->db->delete('tbl_buku');
        }
    
}