<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Siswa extends CI_Model
{
  	public $table = "tbl_siswa";

	public function getSiswa($id, $idUser){
		$this->db->where('id_user', $idUser);
        if ($id == null) {
            $siswa = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id_siswa', $id);
            $siswa = $this->db->get($this->table)->row();
        }
        return $siswa;
    }

    public function updatesiswa($id, $data)
        {
            $this->db->where('id_siswa', $id);
            $this->db->update($this->table, $data);
        }


        public function cekId($id)
        {
            $this->db->where('id_siswa', $id);
            $data = $this->db->get('tbl_siswa')->row();
            return $data;
        }

        function seletctby($id){
            $data = $this->db->get_where('tbl_siswa', ['id_siswa'=>$id]);
            return $data;
        }



        public function deletesiswa($id)
        {
            $this->db->where('id_siswa', $id);
            $this->db->delete('tbl_siswa');
        }

        public function deleteEmail($email)
        {
            $this->db->where('email', $email);
            $this->db->delete('tbl_user');
        }
    
}