<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Transaksi extends CI_Model
{
    function getdata(){
        // if ($id == null) {
        //     $query = "SELECT *
        //             FROM tbl_transaksi
        //             INNER JOIN tbl_siswa USING (id_siswa) WHERE id_user ='$idUser'";
        //             return  $this->db->query($query)->result();

        // }else {
        //     $query = "SELECT *
        //             FROM tbl_transaksi
        //             INNER JOIN tbl_siswa USING (id_siswa) WHERE id_user = '$idUser' AND id_siswa ='$id' ";
        //             return  $this->db->query($query)->row();

        // }
        $query = "SELECT *
                    FROM tbl_transaksi
                    INNER JOIN tbl_siswa USING (id_siswa)";
        return  $this->db->query($query)->result();
        
        // $this->db->where('id_user', $idUser);
        // if ($id == null) {
        //     $siswa = $this->db->get($this->table)->result();
        // } else {
        //     $this->db->where('id_siswa', $id);
        //     $siswa = $this->db->get($this->table)->row();
        // }
        // return $siswa;
    }

    function getTran(){
        $status='dipinjam';
        $query = "SELECT *
                    FROM tbl_transaksi
                    INNER JOIN tbl_siswa USING (id_siswa) WHERE status='$status'";
        return  $this->db->query($query)->result();
    }

    function getTrankem(){
        $status='dikembalikan';
        $query = "SELECT *
                    FROM tbl_transaksi
                    INNER JOIN tbl_siswa USING (id_siswa) WHERE status='$status'";
        return  $this->db->query($query)->result();
    }

    public function cekId($id)
    {
        $this->db->where('id_transaksi', $id);
        $data = $this->db->get('tbl_transaksi');
        return $data;
    }

    function deletetran($id){
        $this->db->where('id_transaksi', $id);
        $this->db->delete('tbl_transaksi');
    }

    function get_nis($nis){
        //$this->db->where('siswa_nis', $nis);
       return $this->db->get_where('tbl_siswa',['siswa_nis'=>$nis])->result();
    }

    function edittran($id){
        $status = 'dikembalikan';
        $this->db->where('id_transaksi',$id);
        $this->db->update('tbl_transaksi',['status'=>$status]);
    }

}