<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
 

class Siswa extends RestController
    {

        function __construct(){
            parent::__construct();
            $this->load->library('validation');
            $this->validation->validationToken();
            $this->load->model('m_siswa');
        }
    
        public function index_get($id = null){
        
            $idUser = $this->validation->validationToken()->id_user;
            $Siswa = $this->m_siswa->getSiswa($id, $idUser);
            if ($Siswa) {
               $this->response( [
                    'status' => true,
                    'message' => 'Produk Berhasi ditemukan',
                    'data' => $Siswa
                    ], 200);
            } else {
                $this->response( [
                    'status' => false,
                    'message' => 'Produk Tidak ditemukan'
                    ], 404);
            }
        }

        public function index_post(){
        
            $config['upload_path']          = './asset/img/';
            $config['allowed_types']        = 'gif|jpg|png';
            $nama_image = $_FILES['gambar']['name'];
            $siswa_nis = $this->input->post('siswa_nis');
            $siswa_nama = $this->input->post('siswa_nama');
            $siswa_kelas = $this->input->post('siswa_kelas');
            $siswa_jurusan = $this->input->post('siswa_jurusan');
            $email = $this->input->post('email');
            $idUser = $this->validation->validationToken()->id_user;

            $this->form_validation->set_rules('siswa_nis', 'Siswa Nis', 'required');
            $this->form_validation->set_rules('siswa_nama', 'Siswa Nama', 'required');
            $this->form_validation->set_rules('siswa_kelas', 'Siswa Kelas', 'required|numeric');
            $this->form_validation->set_rules('siswa_jurusan', 'Siswa Jurusan', 'required');
            // $this->form_validation->set_rules('email', 'Email', 'required|valid_email|unique');
            //$this->form_validation->set_rules('gambar', 'Gambar', 'required');
            

            if($this->form_validation->run()== false){
                $this->response( [
                    'status' => false,
                    'message' =>'Mohon lengkapi Dulu'
                ], 404 );
            }


            $lg = [
                'username' => $siswa_nama,
                'email' => $email,
                'password' => sha1($siswa_nis),
                'level' => 'user'
            ];

            $this->load->library('upload', $config);
    
            if (!$this->upload->do_upload('gambar'))
            {
                $this->response( [
                    'status' => false,
                    'message' => 'Data Gagal di Upload'
                ], 404 );
            }
            else
            {
                $data= [
                    "id_user" => $idUser,
                    "siswa_nis" =>$siswa_nis ,
                    "siswa_nama" =>$siswa_nama ,
                    "siswa_kelas" =>$siswa_kelas,
                    "siswa_jurusan" => $siswa_jurusan,
                    "email" => $email,
                    "gambar" => $nama_image
                ];
                
                $this->db->insert('tbl_siswa',$data);
                $this->db->insert('tbl_user',$lg);
                $this->response( [
                    'status' => true,
                    'message' => 'Data berhasil di Upload'
                ], 200 );
            }
        }
    
    
    
        public function changeUploadImage($gambarLama)
        {
            $config['upload_path']          = './asset/img/';
            $config['allowed_types']        = 'gif|jpg|png';
           // var_dump($gambarLama);die;
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('gambar')) 	{
                return $gambarLama;

            }else{
                unlink(FCPATH . '/asset/img/'. $gambarLama);
                return $this->upload->data('file_name');
            }

        }


function index_put(){
    

        $id = $this->input->post('id_siswa'); 
        $this->form_validation->set_rules('siswa_nis', 'Siswa Nis', 'required');
        $this->form_validation->set_rules('siswa_nama', 'Siswa Nama', 'required');
        $this->form_validation->set_rules('siswa_kelas', 'Siswa Kelas', 'required|numeric');
        $this->form_validation->set_rules('siswa_jurusan', 'Siswa Jurusan', 'required');
       // $this->form_validation->set_rules('email', 'Email', 'required|valid_email|unique');
        
            if ($this->m_siswa->cekId($id) == false) {
            $this->response( [
                        'status' => false,
                        'message' => 'Id Tidak Ditemukan'
                            ], 404 );
            }

                $cekId = $this->m_siswa->cekId($id);			  
                $gambarLama = $cekId->gambar;
                
            $data = [			       
                    'siswa_nis' => $this->input->post('siswa_nis'),
                    'siswa_nama' => $this->input->post('siswa_nama'),
                    'siswa_kelas' => $this->input->post('siswa_kelas'),
                    'siswa_jurusan' => $this->input->post('siswa_jurusan'),
                    'email' => $this->input->post('email'),
                    'gambar' => $this->changeUploadImage($gambarLama)
                ];

                $hasil =  $this->m_siswa->updatesiswa($id, $data);
            if ( ! $hasil == True) {
                $this->response( [
                        'status' => true,
                        'message' => 'siswa Berhasil di Edit',
                        'data' => $hasil
                            ], 200);
            }else{
                    $this->response( [
                            'status' => false,
                            'message' => 'siswa Gagal di Edit'
                                ], 404 );
                }

 }
    
            function selectBy_get(){
            $id = $this->get('id_siswa');
            $data = $this->m_siswa->seletctby($id)->result();
                if ($data != 0){
                $this->response( [
                    'status' => true,
                    'message' => 'siswa Berhasil di diambil',
                    'data' => $data
                        ], 200);
                }else{
                $this->response( [
                    'status' => false,
                    'message' => 'siswa Gagal di diambil'
                        ], 404 );
                }
            }
    
            public function index_delete(){

                $id = $this->delete('id_siswa');
                if ($this->m_siswa->cekId($id) == false) {
                       $this->response( [
                                'status' => false,
                                'message' => 'siswa Gagal di di Hapus'
                                    ], 404 );
                } else {
                    
                    $cekId = $this->m_siswa->cekId($id);
                    $email = $cekId->email;  
                    //var_dump($email); die;           
                    $gambarLama = $cekId->gambar;
                    unlink(FCPATH . '/asset/img/' .$gambarLama);
                    $this->m_siswa->deletesiswa($id);
                    $this->m_siswa->deleteEmail($email);
                    $this->response( [
                                    'status' => true,
                                    'message' => 'siswa Berhasil di Hapus'
                                        ], 200 );
                    }    
        
        
                }



    }