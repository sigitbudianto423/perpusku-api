<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Transaksi extends RestController
    {

        function __construct(){
            parent::__construct();
            $this->load->library('validation');
            $this->validation->validationToken();
            $this->load->model('m_siswa');
            $this->load->model('m_transaksi');
        }

        function datatran_get(){
            //$idUser = $this->validation->validationToken()->id_user;
            
            $data = $this->m_transaksi->getdata();
           if($data == ''){
                $this->response( [
                    'status' => true,
                    'message' => 'Transaksi tidak ditemukan',
                    ], 200);
           }else{
            $this->response( [
                'status' => true,
                'message' => 'Transaksi Berhasi ditemukan',
                'data' => $data
                ], 200);
           }
        }
    
    
        function pinjam_get(){
           $data = $this->m_transaksi->getTran();
           if($data == ''){
                $this->response( [
                    'status' => true,
                    'message' => 'Produk tidak ditemukan',
                    ], 200);
           }else{
            $this->response( [
                'status' => true,
                'message' => 'Produk Berhasi ditemukan',
                'data' => $data
                ], 200);
           }
            
        } 
        
        function kembali_get(){
          $data = $this->m_transaksi->getTrankem();
            if($data == ''){
                $this->response( [
                    'status' => true,
                    'message' => 'Produk tidak ditemukan',
                    ], 200);
           }else{
            $this->response( [
                'status' => true,
                'message' => 'Produk Berhasi ditemukan',
                'data' => $data
                ], 200);
           }
        } 


        function tran_post(){
            $id_siswa = $this->input->post('id_siswa');
            $tgl_pinjam = $this->input->post('tgl_pinjam');
            $tgl_kembali = $this->input->post('tgl_kembali');
            $buku = $this->input->post('nama_buku');
            $status = 'dipinjam';
            $idUser = $this->validation->validationToken()->id_user;
            
            $this->form_validation->set_rules('nama_buku', 'Nama Buku', 'required');
            $this->form_validation->set_rules('tgl_pinjam', 'Tanggal Pinjam', 'required');
            $this->form_validation->set_rules('tgl_kembali', 'Tamnggal Kembali', 'required');
            
            if($this->form_validation->run() == false){
                $this->response( [
                    'status' => false,
                    'message' =>'Mohon lengkapi Dulu'
                ], 404 );
            }

            $data = [
                'id_siswa' => $id_siswa,
                'tgl_pinjam' => $tgl_pinjam,
                'tgl_kembali' => $tgl_kembali,
                'buku' => $buku,
                'status' => $status,
                'id_user' =>$idUser  
            ];


            if($data == ''){
                $this->response( [
                    'status' => false,
                    'message' =>'Mohon lengkapi Dulu'
                ], 404 );
            }else {
                $this->db->insert('tbl_transaksi',$data);
                $this->response( [
                    'status' => true,
                    'message' =>'Berhasil ditambahkan'
                ], 200 );
            }
        }    

        function kembalikan_put(){
           $id = $this->put('id_transaksi');
            if ($this->m_transaksi->cekId($id)->num_rows() == false) {
                $this->response( [
                         'status' => false,
                         'message' => 'Transaksi Gagal di di dikembalikan'
                             ], 404 );
         } else {
            $this->m_transaksi->edittran($id);
                    $this->response( [
                                    'status' => true,
                                    'message' => 'transaksi Berhasil di kembalikan'
                                        ], 200 );
        }
        }
    

        function deletetran_delete(){
            $id = $this->delete('id_transaksi');
                if ($this->m_transaksi->cekId($id)->num_rows() == false) {
                       $this->response( [
                                'status' => false,
                                'message' => 'Transaksi Gagal di di Hapus'
                                    ], 404 );
                } else {
                    
                     $cekId = $this->m_transaksi->cekId($id)->row();             
                    // $gambarLama = $cekId->gambar;
                     //unlink(FCPATH . '/asset/img/' .$gambarLama);
                    $this->m_transaksi->deletetran($id);
                    $this->response( [
                                    'status' => true,
                                    'message' => 'transaksi Berhasil di Hapus'
                                        ], 200 );
                    }    
        }


        function getnis_get(){
            $nis=$this->get('siswa_nis'); 
            $data = $this->m_transaksi->get_nis($nis);
            if($data == ''){
                $this->response( [
                    'status' => false,
                    'message' => 'nis tidak ada'  
                        ], 404 );
            }else{
                $this->response( [
                    'status' => true,
                    'message' => 'nis Berhasil di ambil',
                    'data' => $data
                        ], 200 );
            }

        }
    
    }